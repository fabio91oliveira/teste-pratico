# Challenge Android

### Screenshots

![picture](screen1.png)

![picture](screen2.png)

![picture](screen3.png)

## Projeto

- Construir uma tela de listagem e uma tela de detalhe para filmes.

### Tela de Filmes

- Lista de Generos
- https://api.themoviedb.org/3/genre/movie/list?api_key=1f54bd990f1cdfb230adb312546d765d&language=pt-BR
- Lista de Filmes
- https://api.themoviedb.org/3/movie/upcoming?api_key=1f54bd990f1cdfb230adb312546d765d&language=pt-BR&page=1
- Url base imagens: https://image.tmdb.org/t/p/w185/

- Mostrar em cada célula o filme e a imagem correspondente com mínimo de título, data e imagem e gênero.
- Criar um botão para mudar a ordenação de data para crescente / decrescente.

### Tela de Detalhes

- Mostrar o mínimo de título,imagem,data, sinopese.

### Favoritar

- Em cada filme, deve ser possível favoritá-la. Um filme favorito poderá ser visualizado independente de conexão.

---

## Layout livre

Requisitos mínimos:

- Kotlin  
- Versão Android 4.1 (Api level 16)

## Diferenciais 
- Incluir testes unitários

## Api
- Lista de Generos
- https://api.themoviedb.org/3/genre/movie/list?api_key=1f54bd990f1cdfb230adb312546d765d&language=pt-BR
- Lista de Filmes
- https://api.themoviedb.org/3/movie/upcoming?api_key=1f54bd990f1cdfb230adb312546d765d&language=pt-BR&page=1





