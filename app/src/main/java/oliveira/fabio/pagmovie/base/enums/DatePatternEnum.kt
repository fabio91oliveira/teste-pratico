package oliveira.fabio.pagmovie.base.enums

enum class DatePatternEnum(val pattern: String) {
    REGULAR_PATTERN("yyyy-MM-dd"),
    PT_BR_PATTERN("dd/MM/yyyy");
}