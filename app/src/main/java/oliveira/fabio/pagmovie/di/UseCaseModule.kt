package oliveira.fabio.pagmovie.di

import oliveira.fabio.pagmovie.feature.home.movies.favoritemovieslist.domain.usecase.FavoriteMoviesListUseCase
import oliveira.fabio.pagmovie.feature.home.movies.movieslist.domain.usecase.MoviesListUseCase
import oliveira.fabio.pagmovie.feature.moviedetails.domain.usecase.MovieDetailsUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { MoviesListUseCase(get()) }
    factory { MovieDetailsUseCase(get()) }
    factory { FavoriteMoviesListUseCase(get()) }
}