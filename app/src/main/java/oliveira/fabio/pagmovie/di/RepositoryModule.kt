package oliveira.fabio.pagmovie.di

import oliveira.fabio.pagmovie.feature.home.movies.favoritemovieslist.data.repository.FavoriteMoviesListRepository
import oliveira.fabio.pagmovie.feature.home.movies.movieslist.data.repository.MoviesListRepository
import oliveira.fabio.pagmovie.feature.moviedetails.data.repository.MovieDetailsRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory { MoviesListRepository(get()) }
    factory { MovieDetailsRepository(get()) }
    factory {
        FavoriteMoviesListRepository(
            get()
        )
    }
}