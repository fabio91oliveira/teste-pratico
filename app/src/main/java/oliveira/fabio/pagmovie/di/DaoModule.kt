package oliveira.fabio.pagmovie.di

import oliveira.fabio.pagmovie.data.local.source.Database
import oliveira.fabio.pagmovie.data.local.source.provideBuilder
import org.koin.dsl.module

val daoModule = module {
    single { provideBuilder(get()) }
    single { get<Database>().getMovieDao() }
}