package oliveira.fabio.pagmovie.di

import oliveira.fabio.pagmovie.data.remote.api.MovieApi
import oliveira.fabio.pagmovie.data.remote.config.provideApi
import org.koin.dsl.module

val remoteModule = module {
    single { provideApi(MovieApi::class.java) }
}