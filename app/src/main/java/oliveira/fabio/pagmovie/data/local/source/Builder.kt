package oliveira.fabio.pagmovie.data.local.source

import android.content.Context
import androidx.room.Room

fun provideBuilder(context: Context) = Room.databaseBuilder(
    context,
    Database::class.java,
    "PagMovie.db"
)
    .build()