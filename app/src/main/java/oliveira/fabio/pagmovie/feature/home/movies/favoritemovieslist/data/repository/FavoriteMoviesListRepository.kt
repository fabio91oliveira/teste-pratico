package oliveira.fabio.pagmovie.feature.home.movies.favoritemovieslist.data.repository

import io.reactivex.Flowable
import oliveira.fabio.pagmovie.base.extensions.configThread
import oliveira.fabio.pagmovie.data.local.dao.MovieDao
import oliveira.fabio.pagmovie.data.local.entity.MovieEntity

class FavoriteMoviesListRepository(private val movieDao: MovieDao) {
    fun fetchMovies(): Flowable<List<MovieEntity>> =
        Flowable.fromCallable { movieDao.listMovies() }
            .configThread()
}