package oliveira.fabio.pagmovie.feature.moviedetails.presentation.state

sealed class FavoriteState {
    data class ShowFavorite(val isFavorite: Boolean) : FavoriteState()
    data class ShowError(val throwable: Throwable) : FavoriteState()
}