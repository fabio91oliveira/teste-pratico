package oliveira.fabio.pagmovie.feature.home.movies.movieslist.data.repository

import oliveira.fabio.pagmovie.base.extensions.configThread
import oliveira.fabio.pagmovie.data.remote.api.MovieApi

class MoviesListRepository(
    private val movieApi: MovieApi
) {
    fun getGenres() = movieApi.getGenres().configThread()
    fun getMovies() = movieApi.getMovies().configThread()
}