package oliveira.fabio.pagmovie.feature.home.movies.favoritemovieslist.presentation.state

import oliveira.fabio.pagmovie.data.local.entity.MovieEntity

sealed class FavoriteMoviesListState {
    data class ShowSuccess(val moviesList: List<MovieEntity>) : FavoriteMoviesListState()
    object ShowEmptyState : FavoriteMoviesListState()
    data class ShowError(val throwable: Throwable) : FavoriteMoviesListState()
}